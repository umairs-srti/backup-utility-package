# Backup Utility
Store Data state and Timestamps in backup file to resume from the same state where left off. Option to update backup file to latest version, if out-of-date. Works with json format and key:value pairs.


## Installation
1. Install wheel using pip:
    ```bash
    pip3 install wheel
    ```
2. __Method 1:__
    Install the Backup utility package directly from repo using:
    ```bash
    pip3 install git+https://bitbucket.org/umairs-srti/backup-utility-package.git
    ```
    __Method 2:__
    Install the Backup utility package from a path or after cloning repo:
    ```bash
    pip3 install <path_to_utility_package>

    # For Example
    pip3 install path/to/utility_package/
    ```
    Make sure `setup.py` is inside the `utility_package` folder, or it will not install the package.

___Note:__ It is recommended to use conda or python virtual environment for installing the package._
## Usage
#### Creating New Progress File
Using key data provided by user, a new progress file can be created using `open_mode="w"` to write to `progress_path`. Each key has a status and timestamp associated with it. Default status is `REMAINING`.
```py
from utility.progress import Progress

data = ["a.com", "b.com", "c.com"]
with Progress(open_mode="w", data=data, progress_path="my/progress/path.json", log_path="my/log/path.json") as progress:
    progress.stats()
```
#### Adding New Data to Progress File
New data can be added to progress file by using `open_mode="a"` to add/append new  to `progress_path` with status `REMAINING`.
```py
from utility.progress import Progress

data = ["a.com", "b.com", "c.com"]
with Progress(open_mode="a", data=data, progress_path="my/progress/path.json", log_path="my/log/path.json") as progress:
    progress.stats()
```
#### Reading Progress File
Progress file data can be read by using `open_mode="r"`.
```py
from utility.progress import Progress

with Progress(open_mode="r", progress_path="my/progress/path.json", log_path="my/log/path.json") as progress:
    progress.stats()
```
#### Data Status
Data can have specified statuses including `REMAINING`, `ERROR`, `COMPLETED` and `UNKNOWN`.
Above mentioned statuses can be called together by using `ALL` status.

#### Data Query Arguments
Progress file data can be selected using query arguments using `add_arguments` function.
__Arguments__:-
1. `flag`: get selected data only if the given flag matches the `Status` in progress file
2. `timestamp`: get selected data, which satisfies the datetime condition provided by user
3. `subset`: get selected data from a subset of list of keys
4. `regex`: search a regex in all keys

To get the Query Selected data, use the getter `progress.data`
```py
from utility.progress import Progress, Status

with Progress(open_mode="r", progress_path="my/progress/path.json", log_path="my/log/path.json") as progress:
    progress.add_arguments(
        {
            "flag": Status.REMAINING | Status.UNKNOWN,
            "timestamp": {
                ">": "21/07/2021 06:03:12",
                "<=": "30/07/2021 04:20:39",
            },
            "subset": ["a.com", "b.com", "c.com", "d.com"],
            "regex": r"[ab].com"
        }
    )
    selected_data = progress.data
```
#### Updating Data Status in Progress File
The Status of data/key in progress file can be updated using the `update_status` method. For a given key/data, new `Status` is provided, along with the update/rotation delay and backup file count, to backup file to prevent data loss.
```py
from utility.progress import Progress, Status

with Progress(open_mode="r", progress_path="my/progress/path.json", log_path="my/log/path.json") as progress:
    progress.add_arguments(
        {
            "flag": Status.ERROR,
            "timestamp": {
                ">": "21/07/2021 06:03:12",
            },
        }
    )

    # for every selected data in progress file, update its' status to COMPLETED after every 30 seconds, rotate file every 120 seconds, and keep 5 copies of progress file
    for data in progress.data:
        progress.update_status(data, Status.COMPLETED, update_delay=30, rotation_delay=120, backup_count=5)
```
