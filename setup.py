import setuptools


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='utility',
    version='0.1',
    author='Umair Shahab',
    description='Store Data state and Timestamps in backup file to resume from the same state where left off.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='#',
    install_requires=['wheel', 'tldextract', 'python-dateutil'],
    author_email='',
    packages=setuptools.find_packages(),
    zip_safe=False
)
