from utility.progress import Progress, Status
from utility.fileRotator import FileRotator
import random
import time

if __name__ == "__main__":

    data = [f"www.{i}.com" for i in range(5)]
    with Progress(open_mode="w", data=data) as progress:
        progress.stats()
        progress.add_arguments(
            {
                "flag": Status.ALL,
                # "flag": 1,
                # "timestamp": {
                #     "<=": "30/07/2021 04:20:39",
                #     ">": "30/07/2021 04:09:39",
                # },
                # "subset": ["a.com", "b.com", "c.com", "d.com"],
                # "regex": r"[ab].com"
            }
        )
        output_file = FileRotator(".progress/output.json", update_delay=3, rotation_delay=6)

        i = 0
        while i < 100:
            for data in progress.data:
                # do something here
                time.sleep(1)
                ran = random.choice([1,2,4,8])
                output_file.write_json({i:i})
                progress.update_status(data, Status(ran), update_delay=3, rotation_delay=10)

                i += 1

        output_file.write_json({i:i}, forced=True)
