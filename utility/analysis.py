from backup import Datetime, Backup_Init

def list_analysis(l1, l2):
    l1 = set(l1)
    l2 = set(l2)
    
    all = set(l1) | set(l2)
    sim = set(l1) & set(l2)
    dis_sim = all - sim

    print(f"all: {len(all)} | {len(all)/len(all)*100:.2f}%%")
    print(f"similar: {len(sim)} | {len(sim)/len(all)*100:.2f}%%")
    print(f"dis-similar: {len(dis_sim)} | {len(dis_sim)/len(all)*100:.2f}%%")




if __name__ == "__main__":
    list_analysis([1,2,3], [4,2,5])