import threading
import multiprocessing
from typing import Union

class Lock():
    """Create and return No-Lock or Thread-Lock or Process-Lock as Context Manager
    """
    def __init__(self, lock_type:str="thread") -> None:
        """Initialize Lock Type

        Args:
            lock_type (str, optional): Lock Type can have value of ["thread", "process", "none"]. Defaults to "thread".
        """
        self._lock_obj = self._assign_lock(lock_type)

    def _assign_lock(self, lock_type:str) -> Union[threading.Lock, multiprocessing.Lock, None]:
        """Assign Lock according to provided lock type

        Args:
            lock_type (str): Lock Type can have value of ["thread", "process", "none"]

        Returns:
            Union[threading.Lock, multiprocessing.Lock, None]: get Thread lock object or Process lock object or None type
        """
        if lock_type == "thread":
            return threading.Lock()
        elif lock_type == "process":
            return multiprocessing.Lock()
        else:
            return None

    def __enter__(self):
        """Context Manager start. Enables/Aquires Lock
        """
        self._lock_obj = self._lock_on(self._lock_obj)
        return self._lock_obj

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Context Manager end. Disables/Releases Lock
        """
        self._lock_obj = self._lock_off(self._lock_obj)
        return self._lock_obj

    def _lock_on(self, lock):
        """Enable Lock
        """
        if lock is not None:
            lock.acquire()
        return lock

    def _lock_off(self, lock):
        """Disable Lock
        """
        if lock is not None:
            lock.release()
        return lock

if __name__ == "__main__":
    with Lock(lock_type="thread"):
        print(1)
