from utility.file import from_json, to_json, touch_file
import tldextract
from enum import Flag, auto
from datetime import datetime
import logging
import logging.handlers
from dateutil import parser
import operator
import time



# Global variables. Can be changed if needed
BACKUP_FILE:str = ".backup/backup.json"
LOG_FILE:str = ".backup/log"
DATE_FORMAT:str = "%d/%m/%Y %H:%M:%S"
BACKUP_DELAY_TIME:int = 5
BACKUP_DELAY_TIME_DUPLICATED:int = 60

touch_file(LOG_FILE, mkdir=True, mkfile=False)

# log handler which logs info to log file
LOG_HANDLER = logging.handlers.RotatingFileHandler(
    filename=LOG_FILE,
    mode='a',
    maxBytes=10*1024*1024,
    backupCount=2,
    encoding=None,
    delay=0
)
logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s [%(funcName)16s(:%(lineno)3d)] - %(message)s",
    datefmt=DATE_FORMAT,
    handlers=[
        LOG_HANDLER
    ]
)

class Status(Flag):
    """Enum class containing all status Flags for data

    REMAINING: data that has not been executed
    ERROR: data that returns error on execution
    COMPLETED: data that has been executed successfully
    """
    REMAINING = auto()
    ERROR = auto()
    COMPLETED = auto()


class Backup_Init:
    """Class to initialize backup.json with data, status, and timestamp. Used for resuming from where left of, considering status and timestamp.
    """
    def __init__(self, open_mode:str="r", data:list=[], tld_extract:bool=False) -> None:
        """Initialize the backup directory with backup.json and log.txt using the data list provided

        Args:
            open_mode (str, optional): Open mode for file in BACKUP_FILE directory. Expected Values ("r"=read_only, "w"=write_only, "a"=append). Defaults to "r".
            data (list, optional): data values, on which backup and continuation is required. Defaults to Empty List [].
            tld_extract (bool, optional): Flag if for every data entry, tld extraction is required. tld_extract=domain+suffix. Defaults to False.

        Raises:
            TypeError: if type of the 'data' argument provided is not 'list'
        """

        self.data = None

        # if data is not type list, raise error
        if not isinstance(data, list):
            raise TypeError("data type 'list' expected")

        # if tld_extract true, then extract domain+suffix for every data entry
        if tld_extract:
            data = list(set([".".join([tldextract.extract(str(d)).domain, tldextract.extract(str(d)).suffix]) for d in data]))
        else:
            data = [str(d) for d in set(data)]

        # set current time and Status=REMAINING for every data entry
        time_value = datetime.now().strftime(DATE_FORMAT)
        temp_data = {d:{"status":Status.REMAINING.name, "timestamp":time_value} for d in data}

        # if write mode, then overite file with new data
        if open_mode == "w":
            self.data = temp_data
            logging.info(f"Data Written in {BACKUP_FILE} for backup")
            to_json(BACKUP_FILE, self.data)
        # if append mode, then append only new keys to file
        elif open_mode == "a":
            self.data = from_json(BACKUP_FILE)
            temp_data = {k:v for k, v in temp_data.items() if k not in self.data.keys()}
            self.data.update(temp_data)
            logging.info(f"Data Appended in {BACKUP_FILE} for backup")
            to_json(BACKUP_FILE, self.data)
        # if read only mode, read the data from the file
        else:
            self.data = from_json(BACKUP_FILE)
            logging.info(f"Data Read from {BACKUP_FILE} for backup")

    @classmethod
    def remaining(cls, data_all:list, data_completed:list, open_mode:str="a", tld_compare:bool=True):
        """Compare all data with completed data and updates the backup file with the remaining data

        Args:
            data_all (list): All of the data provided. (Union)
            data_completed (list): The data that has been executed and completed
            open_mode (str, optional): Open mode for file in BACKUP_FILE directory. Expected Values ("r"=read_only, "w"=write_only, "a"=append). Defaults to "a".
            tld_compare (bool, optional): Flag to compute tld_extract value before comparision or not. tld_extract=domain+suffix. Defaults to True.

        Returns:
            self: Returns self, and runs init method of class
        """
        remaining_data = Backup_Init.__get_remaining(data_all, data_completed, tld_compare=tld_compare)
        return cls(open_mode=open_mode, data=remaining_data, tld_extract=tld_compare)

    @classmethod
    def __get_remaining(cls, all:list, completed:list, reset:bool=False, tld_compare:bool=False) -> list:
        """Compare all and completed and find the difference remaining. remaining = set(all) - set(completed)

        Args:
            all (list): set of all data. (Union)
            completed (list): subset of all data
            reset (bool, optional): If remaining needs to be reset to all. If reset=True, then remaining=all. Defaults to False.
            tld_compare (bool, optional): Flag to compute tld_extract value before comparision or not. tld_extract=domain+suffix. Defaults to False.

        Returns:
            list: Unique list of remaining data returned
        """
        assert isinstance(all, list) and isinstance(completed, list), "'all list' and 'completed list' must be of instance type <list>"
        if reset:
            return all
        if tld_compare:
            all = [".".join([tldextract.extract(one).domain, tldextract.extract(one).suffix]) for one in all]
            completed = [".".join([tldextract.extract(one).domain, tldextract.extract(one).suffix]) for one in completed]
        remaining = list(set(all) - set(completed))
        return remaining

    """
    TODO: ADD NEW FUNC FOR READ FILE
    data = {}
    for k, v in bak.from_json("data/input/input.json").items():
        if v == "no":
            data[k] = {"status":"ERROR", "timestamp":str(bak.datetime.now())}
        else:
            data[k] = {"status":"COMPLETED", "timestamp":str(bak.datetime.now())}
    bak.to_json(bak.BACKUP_FILE, data)
    """

    def stats(self, verbose:bool=True) -> dict:
        """Show stats of all the data in backup file. Total, Remaining, Completed and Error Percentages

        Args:
            verbose (bool, optional): Display stats on console. Defaults to True.

        Returns:
            dict: Stats count for all Status returned
        """

        # read backup file
        data = from_json(BACKUP_FILE)
        status_key = "status"
        stats = {
            Status.REMAINING.name: 0,
            Status.ERROR.name: 0,
            Status.COMPLETED.name: 0
        }
        # for every data entry, count the occurence of each status
        for value in data.values():
            stats[Status[value[status_key]].name] += 1

        # if verbose, then print stats to console
        if verbose:
            total = sum(stats.values())
            print(f"Total    :\t{total/total*100:6.2f}% | {total}")
            print(f"Completed:\t{stats[Status.COMPLETED.name]/total*100:6.2f}% | {stats[Status.COMPLETED.name]}")
            print(f"Remaining:\t{stats[Status.REMAINING.name]/total*100:6.2f}% | {stats[Status.REMAINING.name]}")
            print(f"Error    :\t{stats[Status.ERROR.name]/total*100:6.2f}% | {stats[Status.ERROR.name]}")

        return stats


class Datetime:
    """Compare 2 datetimes after parsing them into DATE_FORMAT
    """
    def __init__(self, date_time1:str, date_time2:str) -> None:
        """Parses the 2 provided dates into required DATE_FORMAT

        Args:
            date_time1 (str): datetime string 1
            date_time2 (str): datetime string 2
        """
        # parse and format the datetime strings
        self.dt1 = parser.parse(date_time1).strftime(DATE_FORMAT)
        self.dt2 = parser.parse(date_time2).strftime(DATE_FORMAT)

    def compare(self, operator_sign:str) -> bool:
        """Compare the dates by provided operator

        Args:
            operator_sign (str): operator which is used to compare dates

        Returns:
            bool: Returns comparision result (True/False)
        """
        ops = {
                '>': operator.gt,
                '<': operator.lt,
                '>=': operator.ge,
                '<=': operator.le,
                '==': operator.eq,
                '!=': operator.ne,
            }
        return ops[operator_sign](self.dt1, self.dt2)


class Backup(object):
    """Class Decorator that updates the backup file on every iteration of data, by storing status and timestamp

    Args:
        object (function): Receives a function for Decorator
    """
    def __init__(self, continue_flags:Status=Status.REMAINING, timestamp:str=None, date_compare:str=">", subset_data:list=None) -> None:
        """Initialize class decorator

        Args:
            continue_flags (Status, optional): Flags used for continue status. Status can be (REMAINING, ERROR, COMPLETED). Compatible with Boolean operators (&, |, ^, ~). Defaults to Status.REMAINING.
            timestamp (str, optional): timestamp which is used to compare date and time, and execute function on selected data only. If value is None, all the data is selected. Defaults to None.
            date_compare (str, optional): Comparision operator for timestamp argument. Expected Value (<, >, <=, >=, ==, !=). Defaults to ">".
            subset_data (str, optional): Execute Function for only a subset of data present in the backup file. If type is None, the whole data is used. Defaults to None.

        Raises:
            TypeError: continue_level type is required to be Enum class `Status`
        """

        # continue flags has to be of type 'Status'
        if not isinstance(continue_flags, Status):
            raise TypeError("continue_level type 'Status' expected")

        # subset data has to be `None` or of type 'list'
        if (subset_data is not None) and (not isinstance(subset_data, list)):
            raise TypeError("subset_data type 'list' expected")

        # read data from backup file
        self.continue_flags = continue_flags
        self.backup_data = from_json(BACKUP_FILE)

        # keys for data and status
        self.data_key = "backup_data"
        self.status_key = "status"

        # timestamp and subset related variables
        self.timestamp_key = "timestamp"
        self.timestamp = timestamp
        self.date_compare = date_compare
        self.subset_data = subset_data

        # delay time normal and duplicated
        self.old_time = time.time()
        self.old_time_duplicated = time.time()




    def __call__(self, function, *args, **kwargs):
        """On every function call, this wrapper is executed conditionaly. If data exists in backup file, and Status and Date/Time conditions are fulfilled, the function is executed. Otherwise it is skipped.
        """
        def wrapper(*args, **kwargs):
            ret_func = None
            data = None

            # if backup_data argument is not provided in function, raise exception
            if self.data_key in kwargs:
                data = str(kwargs[self.data_key])
                del kwargs[self.data_key]
            else:
                raise TypeError(f"missing {self.backup_data} from function arguement")

            # if data is in backup file check status and timestamp conditions. otherwise log error in log file
            if data in self.backup_data.keys():
                file_status = self.backup_data[data][self.status_key]

                # if status from data matches the continue flags provided, then check timestamp
                if Status[file_status] in self.continue_flags:
                    time_correct = False

                    # boolean flag for if data is among the accepted subset of data
                    subset_exist = True if (self.subset_data is None) else (data in self.subset_data)

                    # if timestamp is not provided, date/time check not required
                    if self.timestamp is None:
                        time_correct = True
                    # otherwise compare the datetime in file with the datetime provided by user, and check if comdition is fulfilled
                    else:
                        file_timestamp = self.backup_data[data][self.timestamp_key]
                        time_correct = Datetime(file_timestamp, self.timestamp).compare(self.date_compare)

                    # if datetime condition fulfilled and data is within accepted subset, run the function and update new status in backup file
                    if time_correct and subset_exist:
                        ret_func = function(*args, **kwargs)
                        ret_status, prev_status = None, None

                        # if function returns integer as status code
                        if isinstance(ret_func, int):
                            try:
                                ret_status = Status(ret_func).name
                                if ret_status is None:
                                    raise ValueError
                                prev_status = Status[file_status].name
                            # raise error if integer not within required range
                            except ValueError as e:
                                raise ValueError(f"Invalid range {ret_func} returned. Expected {Status.__name__}: {[s.value for s in Status]}")

                        # if function returns Status Enum as status code
                        elif isinstance(ret_func, Status):
                            ret_status = ret_func.name
                            prev_status = file_status
                        # if not type 'Status' or 'int', raise error
                        else:
                            raise TypeError(f"Invalid type `{type(ret_func)}` returned. Expected `int` or `Status`")

                        # update new changes in data
                        time_value = datetime.now().strftime(DATE_FORMAT)
                        self.backup_data[data][self.status_key] = ret_status
                        self.backup_data[data][self.timestamp_key] = time_value

                        # get current new time
                        new_time = time.time()

                        # write changes to original backup file if time delay completed
                        if int(new_time - self.old_time) >= BACKUP_DELAY_TIME:
                            self.old_time = new_time
                            to_json(BACKUP_FILE, self.backup_data)

                        # write changes to duplicated backup file if time delay completed
                        if int(new_time - self.old_time_duplicated) >= BACKUP_DELAY_TIME_DUPLICATED:
                            self.old_time_duplicated = new_time
                            to_json(f"{BACKUP_FILE}.bak", self.backup_data)

                        # log and print data key, old status and new status to log file
                        logging.info(f"key='{data}' | {self.status_key}={prev_status}->{ret_status}")
                        print(f"{ret_status:10s} | {data}")
            else:
                logging.error(f"key '{data}' not found in {BACKUP_FILE}")

            return ret_func
        return wrapper
