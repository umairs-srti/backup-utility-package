import logging
from logging.handlers import RotatingFileHandler
from utility.file import touch_file


def setup_logger(log_path:str, logger_name:str, date_format:str="%d/%m/%Y %H:%M:%S", to_console:bool=True, to_file:bool=True, max_file_size:int=20, backup_file_count:int=1) -> logging.getLogger:
    """Setup a console/file Logger

    Args:
        log_path (str): Path where logging file is saved
        logger_name (str): The name of the Logger
        date_format (str, optional): Format of date to be displayed while logging. Defaults to "%d/%m/%Y %H:%M:%S".
        to_console (bool, optional): Log info to console or not. Defaults to True.
        to_file (bool, optional): Log info to file or not. Defaults to True.
        max_file_size (int, optional): Maximum File Size (in MBs) for RotatingFileHandler. Defaults to 20.
        backup_file_count (int, optional): Number of backup files allowed. Defaults to 1.

    Returns:
        logging.getLogger: Initialize logger and get the object
    """
    # create dir/file if not exist
    touch_file(log_path)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(logger_name)

    logger.propagate = False
    for handler in logger.handlers:
        handler.close()
        logger.removeHandler(handler)

    # add formatting to logger
    log_format = logging.Formatter("[%(asctime)s] %(levelname)6s [%(funcName)16s(:%(lineno)3d)] - %(message)s", date_format)

    # if to_console is true, enable logging to console using StreamHandler
    if to_console:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(log_format)
        stream_handler.setLevel(logging.INFO)
        logger.addHandler(stream_handler)

    # if to_file is true, enable logging to file using RotatingFileHandler
    if to_file:
        info_handler = RotatingFileHandler(
                filename=log_path,
                mode='a',
                maxBytes=max_file_size*1024*1024,
                backupCount=backup_file_count,
                encoding=None,
            )
        info_handler.setFormatter(log_format)
        info_handler.setLevel(logging.INFO)
        logger.addHandler(info_handler)

    return logger

if __name__ == "__main__":
    a = setup_logger("/tmp/a/a.log", "a", to_console=True, to_file=True)
    b = setup_logger("/tmp/a/b.log", "a", to_console=True, to_file=True)
    a.info("123")
    a.info("abc")
