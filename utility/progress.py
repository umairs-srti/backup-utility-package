from utility.fileRotator import FileRotator
from utility.logger import setup_logger
import tldextract
from enum import Flag, auto
from datetime import datetime
from dateutil import parser
import operator
import re


class Status(Flag):
    """Enum class containing all status Flags for data

    REMAINING: flag for data that has not been executed
    ERROR: flag for data that returns error on execution
    COMPLETED: flag for data that has been executed successfully
    UNKNOWN: flag for data of which status is not known or is unexpected
    ALL: flag for everything enabled togather
    """
    REMAINING = auto()
    ERROR = auto()
    COMPLETED = auto()
    UNKNOWN = auto()
    ALL = REMAINING | ERROR | COMPLETED | UNKNOWN


class Progress:
    def __init__(self, open_mode:str="r", data:list=[], progress_path:str=".progress/progress.json", log_path:str=".log/log", tld_extract:bool=True, lock_type:str="none", date_format:str="%d/%m/%Y %H:%M:%S", **kwargs) -> None:
        """Initialize Progress file with data

        Args:
            open_mode (str, optional): Mode of Opening file. ["r" (read), "w" (write), "a" (append)]. Defaults to "r".
            data (list, optional): Data provided as key, which is used to store progress. Defaults to [].
            progress_path (str, optional): Path to where progress is saved. Defaults to ".progress/progress.json".
            log_path (str, optional): Path to where log file is saved. Defaults to ".log/log".
            tld_extract (bool, optional): Enable tld extraction on provided Data list. Defaults to True.
            lock_type (str, optional): Lock Type can have value of ["thread", "process", "none"]. Defaults to "none".
            date_format (str, optional): Format of date in Progress file. Defaults to "%d/%m/%Y %H:%M:%S".
        """

        self.__progress_file_data = None
        self.__date_format = date_format
        self.__arguments = {"flag": Status.REMAINING}

        # create a File Rotator object for backing up and updating progress file
        self.__progress_file = FileRotator(progress_path,
                                        lock_type=lock_type)

        # log handler which logs info to log file
        self.__logger = setup_logger(log_path=log_path,
                                    logger_name="progress_log",
                                    date_format=self.__date_format,
                                    to_console=kwargs.get('log_to_console', True),
                                    to_file=kwargs.get('log_to_file', True),
                                    max_file_size=kwargs.get('max_log_file_size', 20),
                                    backup_file_count=kwargs.get('log_files_count', 1))

        # run update on progress data with selected open mode
        self.__logger.info("Program Execution Started")
        self.__update_file(open_mode, data, tld_extract)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """On exit of context manager, close file safely
        """
        self.__progress_file.write_json(self.__progress_file_data, forced=True)
        self.__logger.info(f"Updated Backup file '{self.__progress_file.path}'")
        self.__logger.info(f"Program Execution Finished")

    def exit(self):
        self.__exit__(None, None, None)

    def __update_file(self, open_mode:str, data:dict, tld_extract:bool) -> None:
        """Update Progress file according to open mode

        Args:
            open_mode (str): Mode of Opening file. ["r" (read), "w" (write), "a" (append)]
            data (dict): Data provided as key, which is used to store progress.
            tld_extract (bool): Enable tld extraction on provided Data list.
        """

        # if writing or appending to file
        if open_mode in ["w", "a"]:
            # if data is not type list, raise error
            self.__type_check(data, list)

            # if tld_extract true, then extract domain+suffix for every data entry
            if tld_extract:
                data = list(set([".".join([tldextract.extract(str(d)).domain, tldextract.extract(str(d)).suffix]) for d in data]))
            else:
                data = [str(d) for d in set(data)]

            # set current time and Status=REMAINING for every data entry
            time_value = datetime.now().strftime(self.__date_format)
            temp_data = {d:{"status":Status.REMAINING.name, "timestamp":time_value} for d in data}

            # if write mode, then overite file with new data
            if open_mode == "w":
                self.__progress_file_data = temp_data
                self.__logger.info(f"Data Written to '{self.__progress_file.path}'")
                self.__progress_file.write_json(self.__progress_file_data, forced=True)

            # if append mode, then append only new keys to file
            elif open_mode == "a":
                self.__progress_file_data = self.__progress_file.read_json()
                temp_data = {k:v for k, v in temp_data.items() if k not in self.__progress_file_data.keys()}
                self.__progress_file_data.update(temp_data)
                self.__logger.info(f"Data Appended to '{self.__progress_file.path}'")
                self.__progress_file.write_json(self.__progress_file_data, forced=True)

        # if read only mode, read the data from the file
        else:
            self.__progress_file_data = self.__progress_file.read_json()
            self.__logger.info(f"Data Read from '{self.__progress_file.path}'")


    def stats(self, verbose:bool=True) -> dict:
        """Show stats of all the data in progress file. Total, Remaining, Completed and Error Percentages

        Args:
            verbose (bool, optional): Display stats on console. Defaults to True.

        Returns:
            dict: Stats count for all Status returned
        """

        # read progress file
        data = self.__progress_file.read_json()

        status_key = "status"
        stats = {
            Status.REMAINING.name: 0,
            Status.ERROR.name: 0,
            Status.COMPLETED.name: 0,
            Status.UNKNOWN.name: 0
        }
        # for every data entry, count the occurence of each status
        for value in data.values():
            stats[Status[value[status_key]].name] += 1

        # if verbose, then print stats to console
        if verbose:
            total = max(sum(stats.values()), 1)
            print(f"TOTAL    :\t{100:6.2f}% | {total}")
            for stat in stats:
                print(f"{stat:9s}:\t{stats[stat]/total*100:6.2f}% | {stats[stat]}")
        return stats

    def __type_check(self, found, expected):
        """Check Typing and raise error if incorrect

        Args:
            found ([type]): Type of data recieved
            expected ([type]): Type of data expected

        Raises:
            TypeError: Raise TypeError if incorrect typing
        """
        if not isinstance(found, expected):
            error_str = f"'{expected.__name__}' expected but '{type(found).__name__}' found."
            self.__logger.exception(error_str)
            raise TypeError(error_str)


    def add_arguments(self, arguments:dict={"flag": Status.REMAINING}) -> None:
        """Arguments that need to be check against progress file data

        Args:
            arguments (dict, optional): User provided Arguments accepts value ["flag":(Status), "regex":(str), "subset":(list), "timestamp":(dict)]. Defaults to {"flag": Status.REMAINING}.

        """
        self.__type_check(arguments, dict)
        self.__arguments = arguments

    def __check_arguments(self) -> list:
        """Check all argument provided against the progress file data

        Returns:
            list: Progress file data keys that satisfy the conditional arguments
        """
        conditional_data = []

        # get every data from progress file
        for data_key, data_val in self.__progress_file_data.items():
            condition_satisfy = True

            # for every argument passed by user, check if data passes the argument
            for arg_key, arg_val in self.__arguments.items():

                # if arg is 'flag', check if status provided matches the status in progress file
                if arg_key == "flag":
                    self.__type_check(arg_val, Status)
                    data_status = Status[data_val["status"]]
                    # if flag matching failed, go to next data in progress file
                    if data_status not in arg_val:
                        condition_satisfy = False
                        break

                # if arg is 'regex', check if regex is found in data
                if arg_key == "regex":
                    self.__type_check(arg_val, str)
                    regex_str = arg_val
                    # if regex not found, go to next data in progress file
                    if not re.search(regex_str, data_key):
                        condition_satisfy = False
                        break

                # if arg is 'subset', check if data is within the given subset
                if arg_key == "subset":
                    self.__type_check(arg_val, list)
                    # if data is not part of subset, go to next data in progress file
                    if data_key not in arg_val:
                        condition_satisfy = False
                        break

                # if arg is 'timestamp', check if datetime provided passes the condition with timestamp of data
                if arg_key == "timestamp":
                    self.__type_check(arg_val, dict)
                    # check this for every timestamp and operator provided by user
                    for operator, timestamp in arg_val.items():
                        self.__type_check(operator, str)
                        self.__type_check(timestamp, str)
                        data_timestamp = data_val["timestamp"]
                        # if datetime comparision failed, go to next data in progress file
                        if not Datetime(data_timestamp, timestamp, self.__date_format).compare(operator):
                            condition_satisfy = False

            # if all conditions satisfy store data key and return it
            if condition_satisfy:
                conditional_data.append(data_key)
        return conditional_data

    def update_status(self, key:str, status:Status, update_delay:int=60, rotation_delay:int=600, backup_count:int=3) -> None:
        """Update new Key status in progress file data

        Args:
            key (str): New data key, that need its status to be changed.
            status (Status): The new Status to be set to. Possible Values [Status.REMAINING, Status.ERROR, Status.COMPLETED, Status.UNKNOWN].
            update_delay (int, optional): Time Delay (in Seconds) for file to get updated. Defaults to 60.
            rotation_delay (int, optional): Time Delay (in Seconds) for file to get rotated. Defaults to 600.
            backup_count (int, optional): Number of backup files to be kept while rotating files. Defaults to 3.
        """

        # check key and status typing
        self.__type_check(status, Status)
        self.__type_check(key, str)

        # edit update/rotate delay and backup count varibles of FileRotator
        self.__progress_file.update_delay = update_delay
        self.__progress_file.rotation_delay = rotation_delay
        self.__progress_file.backup_count = backup_count

        # update new changes in data in progress file, by editing status and timestamp
        time_value = datetime.now().strftime(self.__date_format)
        prev_status = self.__progress_file_data[key]["status"]
        self.__progress_file_data[key]["status"] = status.name
        self.__progress_file_data[key]["timestamp"] = time_value
        updated, rotated = self.__progress_file.write_json(self.__progress_file_data)

        # log info if data updated/rotated and new key status
        if updated:
            self.__logger.info(f"Updated Backup file '{self.__progress_file.path}'")
        if rotated:
            self.__logger.info(f"Rotated Backup file '{self.__progress_file.path}'")
        self.__logger.info(f"key='{key}' | status={prev_status}->{status.name}")

    @property
    def data(self):
        return self.__check_arguments()

    @property
    def file_path(self):
        return self.__progress_file.path

    @property
    def progress_file_data(self):
        return self.__progress_file_data

    @progress_file_data.setter
    def progress_file_data(self, value):
        self.__type_check(value, dict)
        self.__progress_file_data = value

class Datetime:
    """Compare 2 datetimes after parsing them into DATE_FORMAT
    """
    def __init__(self, date_time1:str, date_time2:str, date_format:str) -> None:
        """Parses the 2 provided dates into required DATE_FORMAT

        Args:
            date_time1 (str): datetime string 1
            date_time2 (str): datetime string 2
        """
        # parse and format the datetime strings
        self.__date_format = date_format
        self.dt1 = parser.parse(date_time1).strftime(self.__date_format)
        self.dt2 = parser.parse(date_time2).strftime(self.__date_format)

    def compare(self, operator_sign:str) -> bool:
        """Compare the dates by provided operator

        Args:
            operator_sign (str): operator which is used to compare dates

        Returns:
            bool: Returns comparision result (True/False)
        """
        ops = {
                '>': operator.gt,
                '<': operator.lt,
                '>=': operator.ge,
                '<=': operator.le,
                '==': operator.eq,
                '!=': operator.ne,
            }
        return ops[operator_sign](self.dt1, self.dt2)
