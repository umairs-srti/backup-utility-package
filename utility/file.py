import json
from pathlib import Path
import logging

def from_json(path:str) -> dict:
    """Read JSON file safely

    Args:
        path (str): Path to JSON file to read from

    Returns:
        dict: Get the JSON data
    """

    data = {}
    # if path does not exists, log exception and return empty dict
    if not Path(path).exists():
        logging.exception(f"path '{path}' does not exist")
        return data
    with open(path, "r") as f:
        # if file is not empty, read it
        if Path(path).stat().st_size != 0:
            data = json.load(f)
    return data

def to_json(path:str, data:dict) -> None:
    """Write data to JSON file

    Args:
        path (str): File where JSON data is dumped
        data (dict): Data that is to be dumped in JSON file

    Raises:
        KeyboardInterrupt: After catching KeyboardInterrupt, safely close file and raise another KeyboardInterrupt
    """
    try:
        # create folder directory if it not exists
        path = touch_file(path, mkdir=True, mkfile=False)
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
    # in case of KeyboardInterrupt, safely close file and raise another KeyboardInterrupt
    except KeyboardInterrupt as e:
        to_json(path, data)
        logging.exception(f"KeyboardInterrupt: file `{path}` closed safely.")
        raise e

def touch_file(path:str, mkdir:bool=True, mkfile:bool=True) -> Path:
    """Create Folder/File path if it does not exist

    Args:
        path (str): Directory/File to be created
        mkdir (bool, optional): Make a directory. Defaults to True.
        mkfile (bool, optional): Make a file. Defaults to True.

    Returns:
        Path: Get the Path object
    """
    path = Path(path)
    # if mkdir, create a directory
    if mkdir:
        path.parent.mkdir(parents=True, exist_ok=True)
    # if mkfile, create a file
    if mkfile and path.exists:
        path.touch()
    return path
