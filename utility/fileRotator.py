import os
from pathlib import Path
from typing import Tuple
import time
import datetime
import shutil
import logging
import utility.lock as lock
import utility.file as file

class FileRotator:
    """Store and Rotate file for backup
    """
    def __init__(self, path:str, update_delay:int=60, rotation_delay:int=600, backup_count:int=3, path_modifier:str="%Y%m%d-%H%M%S", lock_type:str="none") -> None:
        """Initialize File Rotator

        Args:
            path (str): Path where file Rotation is done and stored
            update_delay (int, optional): Time Delay (in Seconds) for file to get updated. Defaults to 60.
            rotation_delay (int, optional): Time Delay (in Seconds) for file to get rotated. Defaults to 600.
            backup_count (int, optional): Number of backup files to be kept while rotating files. Defaults to 3.
            path_modifier (str, optional): Postfix date/time modifier for rotated file. Defaults to "%Y%m%d-%H%M%S".
            lock_type (str, optional): Lock Type can have value of ["thread", "process", "none"]. Defaults to "none".
        """

        # create directory if not exist
        self.__path = Path(path)
        file.touch_file(self.__path)
        self.__path_modifier = path_modifier

        self.__update_delay = update_delay
        self.__rotation_delay = rotation_delay

        self.__update_time = time.time()
        self.__rotation_time = time.time()

        self.__backup_count = backup_count

        self.__lock_type = lock_type

    # getter and setters
    @property
    def path(self) -> Path:
        return self.__path
    @path.setter
    def path(self, value:str) -> None:
        self.__path = Path(value)
    @property
    def update_delay(self) -> int:
        return self.__update_delay
    @update_delay.setter
    def update_delay(self, value:int) -> None:
        self.__update_delay = value
    @property
    def rotation_delay(self) -> int:
        return self.__rotation_delay
    @rotation_delay.setter
    def rotation_delay(self, value:int) -> None:
        self.__rotation_delay = value
    @property
    def backup_count(self) -> int:
        return self.__backup_count
    @backup_count.setter
    def backup_count(self, value:int) -> None:
        self.__backup_count = value

    def __time_compare(self, old_time:time.time, delay:int) -> bool:
        """Compare old time with current time, and return if condition True or Not

        Args:
            old_time (time.time): Old value of time to be compared
            delay (int): Delay time to compare with

        Returns:
            bool: If time exceeded the delay time, return True, else False
        """
        current_time = time.time()
        return int(current_time - old_time) >= delay

    def __rotate(self):
        """Rotate file by making backup copy and removing oldest rotation files
        """

        # get current data using path modifier
        date_now = datetime.datetime.now().strftime(self.__path_modifier)
        # append path modifier to the filepath for new file
        new_name = self.__path.parent / f"{self.__path.stem}-{date_now}{self.__path.suffix}"

        # copy from old to new path using thread/process safety
        with lock.Lock(lock_type=self.__lock_type):
            shutil.copyfile(self.__path, new_name)

        # get all rotation files in the path
        get_files = lambda path: [f for f in path.parent.resolve().glob('**/*') if f.is_file() and self.__path.stem in f.name and self.__path.suffix == f.suffix]

        # delete the oldest rotation files, if total files exceed the rotation file limit
        file_list = get_files(self.__path)
        while len(file_list) > self.__backup_count:
            oldest_file = min(file_list, key=os.path.getctime)
            oldest_file.unlink()
            file_list = get_files(self.__path)

    def read_json(self) -> dict:
        """Safely read from file with Lock safety

        Returns:
            dict: JSON Data read from the file
        """
        data = {}
        with lock.Lock(lock_type=self.__lock_type):
            data = file.from_json(self.__path)
        return data

    def write_json(self, data:dict, forced:bool=False) -> Tuple[bool, bool]:
        """Write to JSON file safely, after update delay, with rotation

        Args:
            data (dict): Data that needs to be written to JSON file
            forced (bool, optional): Force writing to JSON file, irrespective to update delay. Defaults to False.

        Raises:
            KeyboardInterrupt: If KeyboardInterrupt caught, safely close file first

        Returns:
            Tuple[bool, bool]: Update and Rotate status. True is file has been Updated/Rotated, False otherwise
        """

        # if no update_delay, set forced update every iteration to True
        if self.__update_delay is None or self.__update_delay == 0:
            self.__update_delay = 0
            forced = True

        # updating and rotating status
        updated = False
        rotated = False

        try:
            # if update is forced, or update delay time is exceeded, then update file
            if self.__time_compare(self.__update_time, self.__update_delay) or forced:
                updated = True
                self.__update_time = time.time()

                # update file with lock safety
                with lock.Lock(lock_type=self.__lock_type):
                    file.to_json(self.__path, data)

            # if rotation delay time is exceeded, then rotate file
            if self.__time_compare(self.__rotation_time, self.__rotation_delay):
                rotated = True
                self.__rotation_time = time.time()
                self.__rotate()

        # if KeyboardInterrupt exception caught, safely write file, then re-raise exception
        except KeyboardInterrupt as e:
            # update file with lock safety
            with lock.Lock(lock_type=self.__lock_type):
                file.to_json(self.__path, data)

            logging.exception(f"KeyboardInterrupt: file `{self.__path}` closed safely.")
            raise e

        # return updated and rotated boolean status
        finally:
            return updated, rotated

if __name__ == "__main__":
    x = FileRotator("/tmp/abc/qq.txt", update_delay=3, rotation_delay=11, backup_count=5)
    for i in range(100):
        x.write_json({i:i})
        if i == 40:
            print(123456)
            x.update_delay = 1
            x.rotation_delay = 2
            x.backup_count = 2
        time.sleep(1)
